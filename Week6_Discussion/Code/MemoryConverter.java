import java.util.Scanner;
public class MemoryConverter
{
    public static void convertMemory(int pageSize, int virtualAddress)
    {
        if (pageSize < 1024 || pageSize > 16384)
        {
            System.out.println("ERROR: Page Size is out of bound!");
            return;
        }
        else
        {
            int pageBits = 0;
            int offsetMask = 0;

            if(pageSize == 1024)
            {
                pageBits = 10;                  // 1024 = 2^10
                offsetMask = 0x000003ff;        // 0011 1111 1111
            }
            else if (pageSize == 2048)
            {
                pageBits = 11;                  // 2048 = 2^11
                offsetMask = 0x000007ff;        // 111 1111 1111
            }
            else if (pageSize == 4096)
            {
                pageBits = 12;                  // 4096 = 2^12
                offsetMask = 0x00000fff;        // 1111 1111 1111
            }
            else if (pageSize == 8192)
            {   
                pageBits = 13;                  // 8192 = 2^13
                offsetMask = 0x00001fff;        // 0001 1111 1111 1111
            }
            else if (pageSize == 16384)
            {
                pageBits = 14;                  // 16384 = 2^14
                offsetMask = 0x00003fff;        // 0011 1111 1111
            }
            else
            {
                System.out.println("ERROR: Invalid Page Size - must be a power of 2!!!");
            }

            // converting the page number and offset
            int offset = (virtualAddress & offsetMask); // using and
            int pageNumber = virtualAddress >> pageBits; // using and and shift

            // displaying the results
            System.out.println("\tThe Address " + virtualAddress + " contains:");
            System.out.println("\t\tPage Number  = " + pageNumber);
            System.out.println("\t\tOffset       = " + offset);
        }
    }

    /**
     * main method to run the program
     * @param args
     */
    public static void main(String[] args)
    {
        if(args.length != 2)
        {
            System.out.println("ERROR: Must Pass 2 arguments!!!");
            System.out.print("\tmem");
            System.out.println(args.length);
            return;
        }
        
        int pageSize = Integer.parseInt(args[0]);
        int virtualAddress = Integer.parseInt(args[1]);
        
        System.out.println("JAVA Address:\t" + pageSize + " - " + virtualAddress);
        convertMemory(pageSize, virtualAddress);
        
        return;
    }
}
